# Ukrainian Elections Telegram Bot

## You can access the bot [here](https://t.me/ua_election_bot).
This is a telegram bot quickly made out of pure interest and curiosity. Its purpose was to poll Telegram users on their stances around 30 questions concerning everything from the *ATO* to digitalization and medical reforms.
>Disclaimer: It does **not** contain the best solutions nor was it designed to do so. 

Hosted on Heroku.

### Made by the_chirik