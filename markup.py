from telebot import types

start_markup = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
start_markup_btn1 = types.KeyboardButton('/start')
start_markup.add(start_markup_btn1)

ok_markup = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
ok_markup.add(types.KeyboardButton('ОК'))

fin_markup = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
fin_markup.add(types.KeyboardButton('Побачити Результат'))

finn_markup = types.ReplyKeyboardMarkup(row_width=0, resize_keyboard=True)

q_markup = types.ReplyKeyboardMarkup(row_width=2, resize_keyboard=True)
q_markup.add(types.KeyboardButton('Так'), types.KeyboardButton('Ні'))


q_deps_markup = types.ReplyKeyboardMarkup(row_width=3, resize_keyboard=True)
q_deps_markup.add(types.KeyboardButton('Так, до 250'), types.KeyboardButton('Так, до 300'), types.KeyboardButton('Ні'))
