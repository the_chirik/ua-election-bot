import telebot
import psycopg2
import markup as m
import tokens as t

party_points = {
    "Сила і честь": 0, # Сила і честь
    "Батьківщина": 0,  # Батьківщина
    "Оппозиційна платформа - За Життя": 0,  # 3
    "Європейська Солідарність": 0,  # 4
    "Слуга Народу": 0,  # 5
    "Голос": 0  # 6
}

coeffs = [[-1,-1,1,-1,-1,-1],[-1,-1,1,-1,-1,-1],[-1,-1,1,-1,-1,-1], [1,1,1,-1,1,0], [1,1,-1,1,1,1], [1,1,-1,1,1,1], [1,1,-1,1,0,1], [1,1,1,1,0,1], [1,1,1,1,-1,-1], [1,1,1,1,1,1], [1,1,-1,1,1,-1], [1,-1,-1,1,-1,1], [1,-1,-1,1,1,1], [1,0,1,1,1,1], [1,1,-1,1,1,1], [1,0,-1,0,1,1], [1,-1,-1,0,-1,-1], [0,0,0,0,0,0], [1,-1,-1,-1,1,1], [1,-1,-1,0,1,1], [0,-1,0,-1,-1,1], [-1,1,-1,-1,0,1], [1,1,-1,1,1,1], [1,1,-1,1,1,1], [-1,1,-1,-1,1,1], [1,-1,1,1,1,1], [1,-1,-1,1,1,1], [1,-1,-1,1,1,1], [-1,1,2,1,2,-1], [-1,-1,-1,1,1,1]]
coeffs.reverse()

bot = telebot.TeleBot(t.TOKEN)
isRunning = False

host = "election-bot.postgres.database.azure.com"
user = "the_chirik@election-bot"
dbname = "postgres"
password = "Kirill2002"
sslmode = "require"

# Construct connection string
conn_string = "host={0} user={1} dbname={2} password={3} sslmode={4}".format(host, user, dbname, password, sslmode)
conn = psycopg2.connect(conn_string)
print("Connection established")


@bot.message_handler(commands=['start', 'go'])
def start_handler(message):
    global isRunning
    if not isRunning:
        chat_id = message.chat.id
        text = message.text
        msg = bot.send_message(chat_id, 'Привіт. я бот який допоможе тобі визначитись, за яку з рейтингуючих партій голосувати на виборах до парламенту України 21 липня.', reply_markup=m.ok_markup)
        bot.register_next_step_handler(msg, askQue1) #OK - only choice
        isRunning = True

def askQue1(message):
    chat_id = message.chat.id
    
    msg = bot.send_message(chat_id, 'Чи мають Українці отримувати послуги та/або голосувати зі свого смартфону?', reply_markup=m.q_markup)
    print(msg)
    cf = 0
    if message.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        print(x)
        party_points[x] = party_points[x]+coeffs[0][i]*cf
        i+=1
    bot.register_next_step_handler(msg, askQue2) #askSource
    return


def askQue2(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Питання скоріше про самих членів партій: Чи можуть депутати мати бізнес, який торгує з РФ?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[1][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue3) #askSource
    return

def askQue3(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи має держава надавати допомогу для розвитку IT-сектора?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[2][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue4) #askSource
    return

def askQue4(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи потрібно державі надавати гідну підтримку ветеранам?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[3][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue5) #askSource
    return

def askQue5(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи має держава надавати допомогу переселенцям?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[4][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue6) #askSource
    return

def askQue6(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи потрібен Україні суд присяжних?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[5][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue7) #askSource
    return

def askQue7(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи потрібна Україні розбудова армії?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[6][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue8) #askSource
    return

def askQue8(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи має курс до НАТО І ЄС стати основою зовнішньополітичного курса країни?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[7][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue9) #askSource
    return

def askQue9(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи вважаєте ви необхідною реформу шкільної освіти?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[8][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue10) #askSource
    return

def askQue10(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи вважаєте ви що Україна повинна припинити брати кредити у ЄС та МВФ?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[9][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue11) #askSource
    return

def askQue11(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи вважаєте ви можливий закон про деолігархізацію або покращення Антимонопольного законодавства долучним кроком у найближчому майбутньому?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[10][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue12) #askSource
    return

def askQue12(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи вважаєте ви поточні антикорупційні закони недостатньо суворими?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[11][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue13) #askSource
    return

def askQue13(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи потрібно змешнити кілкість депутатів в парламенті?', reply_markup=m.q_deps_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[12][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue14) #askSource
    return

def askQue14(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи потрібно замінити парламент України двопалатним парламентом?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[13][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue15) #askSource
    return

def askQue15(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи потрібен орган добору і контролю суддів?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[14][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue16) #askSource
    return

def askQue16(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи потрібне перезавантаження судової системи України?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[15][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue17) #askSource
    return

def askQue17(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи є правильним курс на децентралізацію влади в Україні?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[16][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue18) #askSource
    return

def askQue18(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи потрібно позбавити прав розслідування економічних злочинів правоохоронні органи і створити новий орган фінансової безпеки для діалогу з платниками податків?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[17][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue19) #askSource
    return

def askQue19(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи вважаєте ви поточну медичну реформу правильною?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[18][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue20) #askSource
    return

def askQue20(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи потрібно замінити податок на прибуток податком на виведений капітал для підприємств?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[19][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue21) #askSource
    return

def askQue21(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи потрібно партіям повністю відкривати списки і скасувати мажоритарку?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[20][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue22) #askSource
    return

def askQue22(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи потрібні тимчасові слідчі комісії Верховної Ради України?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[21][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue23) #askSource
    return

def askQue23(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи потрібно зняти недоторканність з Президента та депутатів України?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[22][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue24) #askSource
    return

def askQue24(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи вважаєте закон про люстрацію справедливим?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[23][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue25) #askSource
    return

def askQue25(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи вважаєте ви доцільним закон про декомунізацію?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[24][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue26) #askSource
    return

def askQue26(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи вважаєте ви теперішні закони про мову доцільними?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[25][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue27) #askSource
    return

def askQue27(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи вважаєте ви референдуми доречною формою управління державою?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[26][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue28) #askSource
    return

def askQue28(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, "Чи важаєте Ви амністію бойовиків ОРДЛО доцільним кроком для об'єднання Донбасу?", reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[27][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue29) #askSource
    return

def askQue29(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, 'Чи потрібно знімати економічкну блокаду Донбасу?', reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[28][i] *cf
        i += 1
    bot.register_next_step_handler(msg, askQue30) #askSource
    return

def askQue30(message):
    chat_id = message.chat.id
    msg = bot.send_message(chat_id, "Чи вважаєте Ви прямі перемовини з самопроголошенними ОРДЛО доцільним кроков в урегульованні ситуації на Донбасі?", reply_markup=m.q_markup)
    cf = 0
    if msg.text == 'Так':
        cf = 1
    elif msg.text == 'Ні':
        cf = -1
    else:
        print('Doesnt work as expected')
    i = 0
    for x in party_points:
        party_points[x] = party_points[x]+ coeffs[29][i] *cf
        i += 1
    bot.register_next_step_handler(msg, final) #askSource
    return

def final(message):
    chat_id = message.chat.id
    res_str = 'Спасибі! \nРезультат:\n'
    res_str+=print_res(calc_res(party_points),party_points)
    msg = bot.send_message(chat_id, res_str, reply_markup=m.start_markup)
    bot.register_next_step_handler(msg, start_handler) #askSource
    isRunning = False
    return


bot.polling(none_stop=True)

def calc_res(dict):
    maxx = -999
    for x in dict:
        if x>maxx:
            maxx = x
    return maxx

def print_res(mymax, dict):
    str = ''
    str += 'Партія з программою найбільш до твоїх вподобаннь: '+list(dict.keys())[list(dict.values()).index(mymax)]
    return(str)